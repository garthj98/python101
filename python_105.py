## Dictionaries in Python
# Allow us to keep complex data
    # Mike + no and adrress
    # Anna
# It is very simple, it works like a real dictionary 
# It jumps to th eword you are looking for and has a work for it

# Synatx
# {}
# {"key" : value}
print(type({}))

example_dict ={
    "person1" : "mike", 
    "person2" : "anna"
}

print(example_dict)
print(example_dict["person2"])

# Reasign values
example_dict["person2"] = "Arnold"
print(example_dict)

# Make key value pairs on the fly
example_dict['human47'] = 'Secret Agesnt 047'
print(example_dict)

person1 = {
    'name' : 'Britney',
    'age' : 30,
    'skills' : 'media'
}

print(person1)

person2 = {
    'name' : 'Justin',
    'age' : 34,
    'skills' : 'other'
}

list_of_dict = [person1, person2]
print(list_of_dict)