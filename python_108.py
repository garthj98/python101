## Functions
# Functions are like a machine, they take in arguments, do some work and optup a value
# To work, functions must be called 

# Concepts
# DRY - dont repeat yourself
#   Avoid repetition in your code - functions can help 
#   Maintainable

# Good functions:
#   Are like people - they should have 1 job!
#   Makes it easier to manage, measure and test
#   Important - do not print inside a function you return

# Syntax 
# def <function_name>(arg1, arg2,.... arg*):
#   Block of code
#   return <value>

# Defining a function
def say_hello():
    return 'hello'

# Calling functions 
print(say_hello())

# Function with arg
def say_hello_human(human):
    return "hello " + human

print(say_hello_human('Garth'))

def full_name():
    f_name = input('provide a first name: \n')
    l_name = input('provide a last name: \n')

    return f_name + ' ' + l_name

print(full_name())
