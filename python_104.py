## If conditions 
# If conditions are part of control flow 

# When a condition becomes 'True' it runs a block of code 
# There is also an option to program for an else situation where no condition has become true 
# the syntax - python uses indentation to outline block of code
# meaning after the colon, a block of code starts indentation starts and ends where indentation end 

# if <condition>:
#     <block of code>
# elif <condition>:
#     <block of code>
# else:
#     <block of code>
# 

# Example 
weather = 'Sunny'
# weather = input("what is the weather? ").strip().lower()

print(weather)

# Usually you want to treat user input and format for matching 

if weather == 'Sunny':
    print('Take shades')
elif weather == "windy":
    print("Fly a kite")
else:
    print("weather is not nice")

if True:
    print('It is true')
elif True:
    print("Please print me")
else:
    print('it is not true')

if False:
    print('I will not be printed')
else:
    print('Printing from the else')

## Matching with 'in'
# in python there is a "in" matcher that allows you to match with less restriction 
# works in lists, numbers and strings

lottery_number = 128
your_numbers = [10, 20, 30, 128, 50, 140]

print(128 in your_numbers)
print(lottery_number in your_numbers)

magic_word = "please"

print(magic_word in "where are the cookies")

user_question = input('what would you like? ')
if magic_word in user_question.lower():
    print('You shall have what you wish')
else:
    print('you did not use teh magic word')