# Strings
## Strings are a list on unorganised charatcers that can have spaces, numbers and any characters
## syntax is "" or ''
print('this is a string')
print(type("this too is a string"))

my_string = "this is a SIMPLE string"

# len() tells length of the string 
print(len(my_string))
print(my_string)

# USeful string methods
# object.capitalize() makes the first character upper case
print(my_string.capitalize())

# .lower() changes all cases to lower
print(my_string.lower())

# .upper() changes all cases to upper
print(my_string.upper())

# .title() capitalises the first letter of all words in a string 
print(my_string.title()) 

# COncationation of strings 
name = 'garth'
greeting = 'henlo'

print(greeting + " " + name)
print(greeting, name)

# Interpolation with f strings
# you can interpolate into a string
# syntax print(f"")
print("Welcome and henlo {name}!")
print(f"Welcome and henlo {name}!")
print(f"Welcome and henlo {name.capitalize()}!")

# Strings.. as said in the start are lists of characters. But they are lists.
print(name[0])

# Numerical types
# These are numbers. We have intergers, floats and a few others taht are less used.
# you can perform mathamatical arithmatic with them 

my_number = 16
print(my_number)
print(type(my_number))

a = 8
b = 24

# Add, subtract, divide, multiply and others 
print(a + b)
print(a - b)
print(a * b)
# all of the above are intergers
print(a / b)
# above is a float, floats are composites and every time you divide you always get a float
print(type(a / b))
print(type(a / 4))
print(type(a * b))

# There are comparison operators you can use logically with maths
# such as greater than, smaller than and so on.
# The result is know as a boolean 

# A boolean is a data type that is either True or False.

# Booleans - True or False

print(type(True)) # <class 'bool'>
print(type(False))

### Comparison operators 
# Greater than
print(a > b)
print(type(a > b))

# Smaller than 
print(a < b)

# Greater than or equal 
print(a >= 8)
print(a >= b)

print(a <= 8)
print(a <= b)

# Equal to 
print(a == 8)
print(a == b)

# Not equal to 
print(a != 8)
print(a != b)

## Comparison operators also work for strings
print('is the stringn 10 equal to interger 10 in py?', '10' == 10)

ex_input = 10
user_input = input("> provide input: ")
print(ex_input == user_input)
print(type(ex_input))
print(type(user_input))

## User input is captured as a string 
# Casting - helps us change data type where possible very useful for taking in user input 
# str() changes to string 

ex_int = 101
ex_str = str(ex_int)
print(type(ex_str))

# Change a string into an interger 
st = '3'
st = int(st)
print(type(st))
