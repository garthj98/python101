## For loops

# syntax
# for x in <iterable>:
#   <blockOfCode>

farhiaya_gifts = ['roller blades', 'roller scates', 'vacay']

for item in farhiaya_gifts:
    print(item)

## Embeded Lists
farhiaya_gifts_2 = [['roller blades', 'roller scates', 'vacay'], ['stuffed toy', 'game boy', 'bike']]

for item in farhiaya_gifts_2:
    print(item)
    for ind in item:
        print(ind)

## Lets say we want to create a string with all the tiems 
farhiaya_gifts_2 = [['roller blades', 'roller scates', 'vacay'], ['stuffed toy', 'game boy', 'bike']]

# Lets first iterate over the list then get eaach indivdual item and add it to a string 
# Initialise new variable 

result_s = ''
for item in farhiaya_gifts_2:
    for ind in item:
        print(ind)
        result_s += ind + ' '
print(result_s)

## Iterating over a dictionary 

person2 = {
    'name' : 'Justin',
    'age' : 34,
    'skills' : 'other'
}

for key in person2:
    print(key)
    print(person2[key])

for key in person2.keys():
    print(key)
    print(person2[key])

