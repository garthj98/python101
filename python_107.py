## While loops 
# A loop with a while condition

# Syntax 
# while <condition>:
#   <block of code>

# syntax break in body condition 
# while True:
#   block of code
#   <condition>:
#       break

# counter = 0 
# while counter < 10:
#     print('I am teh while loop')
#     counter += 1

# counter = 0
# while True:
#     print('I am the loop')
#     counter += 1
#     if counter > 10:
#         break

# Create small switch boards - good for small games/apps

while True:
    print('Welcome to teh matrix')
    print('Press 1 for hello')
    print('Print 2 for a surprise')
    print('press 3 for goodbye')
    user_input = int(input('Please choose an option ').strip())
    
    # use user inpu to control flow what program to run 
    if user_input == 1:
        print('hello')
    elif user_input == 2:
        print('terd')
    elif user_input == 3:
        print('goodbye')
        break
