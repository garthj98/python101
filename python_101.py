## print() command
### Print command makes code output to the terminal, super useful for debugging

print('hello world!')

###^^^^6 this does not mean code can not run wothout outputting to the terminal
10*100
20*33
print(20*2) # Only one thats printed

## type()
### Helps you identify the type of data. For example:
print(10)
print(type(10))

print('10')
print(type('10'))

## Variables
### A variable is like a box. It has a name, we can put stuff inside and the get the stuff that is inside.
### We can also at any point put other stuff inside the same box
### We could have a box called books and putch "rick dad, poor dad" inside

#Assignment of varibale books to a string
books = "rich dad, poor dad"
print(books)
print(type(books))

## Prompting user for input
# syntax input('messgae')

input('Tell me a number')

# You need to capture the inputu into a variable for later use

user_var = input('Tell me a number: ')
print(user_var)

## Python errors

# Syntax
# No name
