# Lists 
## A list is a numerically list of obkects. Lists are organised using indexs that start at 0.
## A list can be anything like a shopping list or a list of craxy ex partners/emplyers/landlords

# syntax []
print(type([]))

# You can define a list with serveral items
my_crazy_employers = ['Walt disney', 'looney tunes', 'cartoon network']

# Call the lsit and print everything
print(my_crazy_employers)
# lists are organised with index - starting at 0
# list_name - [0, 1, 2]
# USe indexs to get individual items off a list
print(my_crazy_employers[0])
print(my_crazy_employers[-1])

## Add something to the list 
# .append()
my_crazy_employers.append('channel 4')
print(len(my_crazy_employers))

## Remove something from the list
# .pop()
print(my_crazy_employers.pop(2))
print(my_crazy_employers)

## Lists are mutable and can take multiple data types 

new_list = [10, 11, 20, "hello", "multiple data", [10, 20, "yeah"]]
print(new_list)