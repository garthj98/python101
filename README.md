# Python 101

Python is a programming language that is syntax light and easy to pick up. It is ver versitile and will paid. Used across teh industry for:
- Simple scripts 
- Automation (ansible)
- Web development 
-  Data and ML
-  Others 

## List of thinsg to cover:
- Data types
- Strings 
- Numericals 
- Booleans 
- List 
- Dictoionaries 
- If conditions 
- Loops 
- While loops
- functions
- TDD and unittesting 
- Error handling 
- External packages 
- API's

### Other topics to talk:

- Polymorphsim 
- TDD 
- Dry code 
- Serperation of concerns 
- ETL - Extract transform and load 
- Framworks (difference to actual languages )

## Python Intro and basics 

Python you can make files and run them, you can run on the command line or you can have a framework running python or use an IDE.

To try stuff out use the command line.

```bash
python3
>>> human = 4
>>> print(human)
4
```

## Virtual environment in python venv

.venv folder is a virtual environment for python, where you can install packages with pip locally per project, rather than globally in your machine.

You need to start a venv environment and activate in using `source`.

code

```bash
python3 -m venv .venv
source .venv/bin/activate
```

This is to avoid package collision on your machine and contains the python packages installed to this folder. 

***requirements.txt*** Is a text file where you can specify a list of packages your code will need. You can then load it / read it using pup. It also locks in versions. 

Read it with pip or pip3 using this code:
```bash
pip install -r requirements.txt
```
This will install all the packages in requirements.txt.